import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import SCREENS_NAME from '../constants/screens';
import HomeScreen from '../screens/home';

const Stack = createNativeStackNavigator();

export const TabNavigator = () => {
  return (
    <Stack.Navigator initialRouteName={SCREENS_NAME.HOME_SCREEN}>
      <Stack.Screen
        name={SCREENS_NAME.HOME_SCREEN}
        component={HomeScreen}
        options={{
          headerTitle: 'Home screen',
        }}
      />
    </Stack.Navigator>
  );
};
