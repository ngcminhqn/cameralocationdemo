import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import {TabNavigator} from './TabNavigator';

const Stack = createNativeStackNavigator();

function RootNavigator() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}>
        {
          <Stack.Screen
            name="TabNavigator"
            component={TabNavigator}
            options={{
              animationTypeForReplace: 'push',
            }}
          />
        }
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default RootNavigator;
