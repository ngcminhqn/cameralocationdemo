import React from 'react';
import {Provider} from 'react-redux';
import configureStore from './redux/store/configureStore';
import RootNavigator from './routes/RootNavigator';

const storeConfig = configureStore();

export const store = storeConfig;

const App = () => {
  return (
    <Provider store={store}>
      <RootNavigator />
    </Provider>
  );
};

export default App;
