import {HOME} from 'redux-app/actionTypes';

export const getHomeRequest = payload => ({
  type: HOME.GET_HOME.REQUEST,
  payload,
});

export const getHomeSuccess = payload => ({
  type: HOME.GET_HOME.SUCCESS,
  payload,
});

export const getHomeFailure = payload => ({
  type: HOME.GET_HOME.FAILURE,
  payload,
});
