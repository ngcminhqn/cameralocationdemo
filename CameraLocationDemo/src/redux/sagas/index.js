import {all, fork} from '@redux-saga/core/effects';
import homeSaga from './homeSagas';

export default function* rootSaga() {
  yield all([fork(homeSaga)]);
}
