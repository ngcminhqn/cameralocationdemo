import {applyMiddleware, createStore} from 'redux';
import {createLogger} from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import {composeWithDevTools} from 'redux-devtools-extension';
import rootSaga from '../sagas';
import rootReducer from '../reducers';

let middlewares = [];
const loggerMiddleware = createLogger({
  collapsed: (getState, action, logEntry) => !logEntry.error,
});
const sagaMiddleware = createSagaMiddleware();

middlewares = [...middlewares, loggerMiddleware, sagaMiddleware];

const middleware = composeWithDevTools(applyMiddleware(...middlewares));

export default function configureStore() {
  const store = createStore(rootReducer, middleware);
  sagaMiddleware.run(rootSaga);
  return store;
}
