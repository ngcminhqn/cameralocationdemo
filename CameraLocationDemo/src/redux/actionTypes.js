export const changeTypes = action => ({
  ORIGIN: action,
  REQUEST: `${action}_REQUEST`,
  SUCCESS: `${action}_SUCCESS`,
  FAILURE: `${action}_FAILURE`,
  PENDING: `${action}_PENDING`,
  START: `${action}_START`,
  END: `${action}_END`,
});

export const HOME = {
  GET_HOME: changeTypes('GET_HOME'),
};
