import {HOME} from '../actionTypes';

const initialState = {
  isLoading: false,
  errorMessage: '',
  isError: false,
};

const homeReducer = (draft = initialState, action) => {
  let {payload, type} = action;
  switch (type) {
    case HOME.GET_HOME.REQUEST:
      return {
        ...draft,
        isLoading: true,
        isError: false,
        errorMessage: '',
      };

    case HOME.GET_HOME.SUCCESS:
      return {...draft, isLoading: false, isError: false};

    case HOME.GET_HOME.FAILURE:
      return {...draft, isLoading: false, isError: true, errorMessage: payload};

    default:
      return {...draft};
  }
};

export default homeReducer;
